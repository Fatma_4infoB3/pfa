package edu.app.web.mb;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import edu.app.business.PropertyOwnerServiceLocal;
import edu.app.persistence.PropertyOwner;


@ManagedBean
@ViewScoped
public class PropertyOwnerBean {
	
	
	private PropertyOwner propertyOwner = new PropertyOwner();
	
	@EJB
	private PropertyOwnerServiceLocal propertyServiceLocal;
	
	
	public PropertyOwnerBean() {
	}


	public PropertyOwner getPropertyOwner() {
		return propertyOwner;
	}


	public void setPropertyOwner(PropertyOwner propertyOwner) {
		this.propertyOwner = propertyOwner;
	}
	
	
	
	public String doSaveOrUpdate()
	
	{
		
		
		String navigateTo=null;
		
		 
		propertyServiceLocal.update(propertyOwner); 
		return navigateTo;
		
	}


	
	

}
