package edu.app.web.mb;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.DragDropEvent;
import org.primefaces.event.FlowEvent;

import edu.app.business.AdressService;
import edu.app.business.StudentService;
import edu.app.persistence.Adress;
import edu.app.persistence.RentalAd;
import edu.app.persistence.Student;

@ManagedBean
@SessionScoped
public class collocationBean implements Serializable {

	    /**
	 * 
	 */
	private static final long serialVersionUID = -3430601473621443640L;
	private static Logger logger = Logger.getLogger(collocationBean.class.getName()); 

	@EJB
	private StudentService studentService ;
	@EJB
	private AdressService adressService ;
		
	@EJB
	private ArrayList<RentalAd> rentalAds;

	@EJB
	 private ArrayList<RentalAd> selectedchoices;
	
	    private boolean skip;
	    
@EJB
private Student student = new Student();
@EJB
private RentalAd rentalAd = new RentalAd();

@EJB
private Adress adress = new  Adress();

	    public collocationBean () {
	        rentalAds = new ArrayList<RentalAd>();
	        selectedchoices = new ArrayList<RentalAd>();

	        //rentalAds.add(new RentalAd("zde","dzes", (float) 50.5));
	        //rentalAds.add(new RentalAd("sdc", "description", (float) 521));
	        
	     //   players.add(new RentalAd("Villa", 7, "smallroom.jpg", "forward"));
	
	        


		
		
		
		
	    
	      
	    }

	    
	    public void onDrop(DragDropEvent event) {
	       RentalAd rentalAd = (RentalAd) event.getData();

	        selectedchoices.add(rentalAd);

	        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(rentalAd.getTitle() + " added", "Position:" + event.getDropId()));
	    }
	    
	    
	    
	    
	    public List<RentalAd> getRentalAds() {
			return rentalAds;
		}
	    
	    
	    public List<RentalAd> getSelectedchoices() {
			return selectedchoices;
		}
	    
	 
	    public RentalAd getRentalAd() {
			return rentalAd;
		}
	    
	    public Student getStudent() {
			return student;
		}
	    
	    public void setRentalAd(RentalAd rentalAd) {
			this.rentalAd = rentalAd;
		}
	    
	    public void setSelectedchoices(ArrayList<RentalAd> selectedchoices) {
			this.selectedchoices = selectedchoices;
		}
	    
	    public void setRentalAds(ArrayList<RentalAd> rentalAds) {
			this.rentalAds = rentalAds;
		}
	    
	    public void setStudent(Student student) {
			this.student = student;
		}
	    
	    
	    public Adress getAdress() {
			return adress;
		}
	    
	    public AdressService getAdressService() {
			return adressService;
		}
	    
	    public StudentService getStudentService() {
			return studentService;
		}
	    
	    public static Logger getLogger() {
			return logger;
		}
	    
	    public void setAdress(Adress adress) {
			this.adress = adress;
		}
	    
	    
	    public void setAdressService(AdressService adressService) {
			this.adressService = adressService;
		}
	    
	    public static void setLogger(Logger logger) {
			collocationBean.logger = logger;
		}
	    
	    public void setStudentService(StudentService studentService) {
			this.studentService = studentService;
		}
	    
	    
	    
	    
		public void save(ActionEvent actionEvent) {
			//Persist user
	               
			//adressService.createAdress(adress);
		     
			student.setAdress(adress);
			adress.setStudent(student);
			studentService.update(student);
			//student.addAddress(adress);
			
			//studentService.assignStudentToAdress(students, adress);
			
			//studentService.update(student);
			
			FacesMessage msg = new FacesMessage("Successful", "Welcome :" + student.getFirstname());
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
		
		public boolean isSkip() {
			return skip;
		}

		public void setSkip(boolean skip) {
			this.skip = skip;
		}
		
		public String onFlowProcess(FlowEvent event) {  
	        logger.info("Current wizard step:" + event.getOldStep());  
	        logger.info("Next step:" + event.getNewStep());  
	          
	        if(skip) {  
	            skip = false;   //reset in case user goes back  
	            return "confirm";  
	        }  
	        else {  
	            return event.getNewStep();  
	        }  
	    }  
	    
	}
			
	

