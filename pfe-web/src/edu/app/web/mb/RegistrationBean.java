package edu.app.web.mb;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.Registration;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;

import edu.app.business.AdressService;
import edu.app.business.StudentService;
import edu.app.persistence.Adress;
import edu.app.persistence.Student;


@ManagedBean
@SessionScoped
public class RegistrationBean implements Serializable {



		//private User user = new User();
		
		/**
	 * 
	 */
	private static final long serialVersionUID = 4052061597317802310L;

		private Student student = new Student();
		private Adress adress = new  Adress();
		
		
		
		
		
		 private boolean value1;  
		  
		    private boolean value2;  
		  
		    
		@EJB
		private StudentService studentService ;
		private AdressService adressService ;
		
		private boolean skip;
		
		private final Logger logger = Logger.getLogger(Registration.class.getName());

	public Student getStudent() {
		return student;
	}
	
	
	public void setStudent(Student student) {
		this.student = student;
	}
	
	public Adress getAdress() {
		return adress;
	}
	
	public void setAdress(Adress adress) {
		this.adress = adress;
	}
	
	
	
	public boolean isValue1() {  
        return value1;  
    }  
  
    public void setValue1(boolean value1) {  
        this.value1 = value1;  
    }  
  
    public boolean isValue2() {  
        return value2;  
    }  
  
    public void setValue2(boolean value2) {  
        this.value2 = value2;  
    }  
  
    public void addMessage() {  
        String summary = value2 ? "Checked" : "Unchecked";  
  
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));  
    }  

	
	
	
		public void save(ActionEvent actionEvent) {
			//Persist user
	               
			//adressService.createAdress(adress);
		     
			student.setAdress(adress);
			adress.setStudent(student);
			studentService.update(student);
			//student.addAddress(adress);
			
			//studentService.assignStudentToAdress(students, adress);
			
			//studentService.update(student);
			
			FacesMessage msg = new FacesMessage("Successful", "Welcome :" + student.getFirstname());
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
		
		public boolean isSkip() {
			return skip;
		}

		public void setSkip(boolean skip) {
			this.skip = skip;
		}
		
		public String onFlowProcess(FlowEvent event) {
			logger.info("Current wizard step:" + event.getOldStep());
			logger.info("Next step:" + event.getNewStep());
			
			if(skip) {
				skip = false;	//reset in case user goes back
				return "confirm";
			}
			else {
				return event.getNewStep();
			}
		}


		public AdressService getAdressService() {
			return adressService;
		}


		public void setAdressService(AdressService adressService) {
			this.adressService = adressService;
		}
		
		
		
        public void handleFileUpload(FileUploadEvent event) {  
        	student.setPicture(event.getFile().getContents());
        	student.setPictureName(event.getFile().getFileName());
            FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");  
            FacesContext.getCurrentInstance().addMessage(null, msg);  
        }  
		
	}
	                    

