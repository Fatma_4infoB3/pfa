package edu.app.web.mb;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.event.map.PointSelectEvent;
import org.primefaces.event.map.StateChangeEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.LatLngBounds;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;


@ManagedBean
@SessionScoped
public class MapBean implements Serializable {
	

	private static final long serialVersionUID = 6042419350972608334L;

	/**
	 * 
	 */
	
	private MapModel simpleModel;
	
  
	  
	    private Marker marker;

	public MapBean() {
		simpleModel = new DefaultMapModel();
		
		//Shared coordinates
		LatLng coord1 = new LatLng(36.881406, 10.212436);
		LatLng coord2 = new LatLng(36.899048, 10.189261);
		LatLng coord3 = new LatLng(36.8530977,10.2072001);
		LatLng coord4 = new LatLng(36.885233, 30.702323);
		
		//Basic marker
		simpleModel.addOverlay(new Marker(coord1, "Chotrana"));
		simpleModel.addOverlay(new Marker(coord2, "Esprit ghazela"));
		simpleModel.addOverlay(new Marker(coord3, "Esprit charguia"));
		simpleModel.addOverlay(new Marker(coord4, "Kaleici"));
	}

	public MapModel getSimpleModel() {
		return simpleModel;
	}
	

	public void onStateChange(StateChangeEvent event) {
		LatLngBounds bounds = event.getBounds();
		int zoomLevel = event.getZoomLevel();
		
		addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "Zoom Level", String.valueOf(zoomLevel)));
		addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "Center", event.getCenter().toString()));
		addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "NorthEast", bounds.getNorthEast().toString()));
		addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "SouthWest", bounds.getSouthWest().toString()));
	}
	
	public void onPointSelect(PointSelectEvent event) {
		LatLng latlng = event.getLatLng();
		
		addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "Point Selected", "Lat:" + latlng.getLat() + ", Lng:" + latlng.getLng()));
	}
	
	
	
	
	
	  public void onMarkerSelect(OverlaySelectEvent event) {  
	        marker = (Marker) event.getOverlay();  
	          
	        addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "Marker Selected", marker.getTitle()));  
	    }  
	      
	    public Marker getMarker() {  
	        return marker;  
	    }  
	      
	    public void addMessage(FacesMessage message) {  
	        FacesContext.getCurrentInstance().addMessage(null, message);  
	    }  
	}  
	                      
	
	
	
	
					