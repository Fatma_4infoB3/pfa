package edu.app.web.mb;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


@ManagedBean
@SessionScoped
public class languageBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6157041057791794788L;

	private List<String> selectedLanguages;

    private List<String> selectedOptions;

    private Map<String,String> languages;

    
    
    
    
    
    
    public languageBean() {
       languages = new HashMap<String, String>();
        languages.put("Arabic", "Arabic");
        languages.put("English", "English");
        languages.put("French", "French");
       languages.put("Spanish", "Spanish");
    }

    
    public Map<String, String> getLanguages() {
		return languages;
	}
    
    public List<String> getSelectedLanguages() {
		return selectedLanguages;
	}
    
    
    public List<String> getSelectedOptions() {
		return selectedOptions;
	}
    
    public void setLanguages(Map<String, String> languages) {
		this.languages = languages;
	}
    
    public void setSelectedLanguages(List<String> selectedLanguages) {
		this.selectedLanguages = selectedLanguages;
	}
    
  public void setSelectedOptions(List<String> selectedOptions) {
	this.selectedOptions = selectedOptions;
}
  
    
    
}
                