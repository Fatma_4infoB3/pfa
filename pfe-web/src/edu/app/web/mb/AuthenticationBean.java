package edu.app.web.mb;


import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import edu.app.business.AuthentificationRemote;
//import edu.app.business.AuthenticationServiceLocal;
import edu.app.persistence.User;


@ManagedBean(name="authBean")
@SessionScoped
public class AuthenticationBean implements Serializable {

	
	@EJB
	private AuthentificationRemote authentificationRemote;
	
	private static final long serialVersionUID = 6710404278650523921L;
	
	private User user= new User();
	
	private boolean loggedIn=false;
	

	public AuthenticationBean() {
	}
	
	
	public String login()
	{
		String navigateTo=null;
		
		User found = authentificationRemote.authenticate(user.getLogin(), user.getPassword());
		
		if (found!=null) {
			user= found;
			loggedIn = true;
			navigateTo= "/pages/admin/adminHome";
			
		} else {
			
			FacesMessage message = new FacesMessage("Bad credentials!!");
			FacesContext.getCurrentInstance().addMessage("login_form:login_submit",message);
			loggedIn = false;
			navigateTo=null;

		}
		
		return navigateTo;
		
		
	}


	
	 
	
	public String logout(){
		
		String navigateTo=null;
		loggedIn=false; 
		user = new User();
		navigateTo="/welcome";
		
		return navigateTo;
	}
	
	
	

	public AuthentificationRemote getAuthentificationRemote() {
		return authentificationRemote;
	}


	public void setAuthentificationRemote(
			AuthentificationRemote authentificationRemote) {
		this.authentificationRemote = authentificationRemote;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public boolean isLoggedIn() {
		return loggedIn;
	}


	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	
	
	
	
	
}
